﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using Photon.Pun;
using UnityEngine.UI;


/// <summary>
/// Control for the player not in VR (Operator)
/// </summary>
public class ClassicPlayerControl : MonoBehaviour
{

    // Rotation for the camera to look up and down
    private float xRotation = 0;

    private PhotonView view;

    // The operator's camera
    [Tooltip("The operator's camera")]
    public Camera cam;

    // The transform of the camera
    private Transform camTrans;

    // Start is called before the first frame update
    void Start()
    {
        view = gameObject.GetComponent<PhotonView>();
        camTrans = cam.GetComponent<Transform>();

        // If it is not the operator's view, we deactivate the camera to prevent "camera switch" between players
        if(!view.IsMine)
        {
            cam.enabled = false;
        }

        // Place the operator's camera as the worldcamera for the canvas to allow interactions with the canvas' buttons
        GameObject canvasObj = GameObject.FindGameObjectWithTag("CanvasOperator");
        canvasObj.GetComponent<Canvas>().worldCamera = cam;
    }

    // Update is called once per frame
    void Update()
    {
        if(view.IsMine)
        {
            float mouseX = Input.GetAxis("Mouse X");
            float mouseY = Input.GetAxis("Mouse Y");

            // the minus is here to not have reversed control of the camera up and down
            xRotation -= mouseY;

            // Clamping so that we can not do a complete rotation arround the x axis
            xRotation = Mathf.Clamp(xRotation, -90, 90);

            // Shift is here to freeze the rotation of the operator, to easily click on buttons
            if(!Input.GetKey(KeyCode.LeftShift))
            {

                // We change the CAMERA rotation arround the x axis (to look up and down)
                camTrans.localRotation = Quaternion.Euler(xRotation, 0, 0);

                // We change the OPERATOR (capsule) rotation arround the y axis (left and right)
                transform.Rotate(Vector3.up, mouseX);
            }

            // We translate the position of the operator (capsule) with the axis (directionnal arrows) 
            transform.Translate(Input.GetAxis("Horizontal") * 5 * Time.deltaTime, 0, Input.GetAxis("Vertical") * 5 * Time.deltaTime);
        }
    }
}
