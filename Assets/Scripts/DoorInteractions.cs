﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.XR.Interaction.Toolkit;

public class DoorInteractions : MonoBehaviour
{
    public Animator anim;

    private bool isOpen = false;

    // Start is called before the first frame update

    public void OpenDoor()
    {
        isOpen = !isOpen;
        anim.SetBool("isOpen", isOpen);
    }

    private void OnMouseOver()
    {
        OpenDoor();
    }
}
