# Details

To have a better understanding of the project, you can go on https://jfl.com/mixed-reality-therapy/ and refer to this paper from Dr. John Francis Leader 
https://ieeexplore.ieee.org/abstract/document/8516530 

# Disclaimer

Important: This code is an unfinished prototype. It is provided without warranty and liability. It is intended to be used initially for concept development and testing purposes and, following successful testing and verification, for clinical use only under the guidance of a suitably trained and licensed mental health professional with experience in its design and function and with suitable safeguards in place.

# How to use

This project purposes is to allow people to perform a therapy session in virtual reality. The targeted here device is the Oculus Quest.
The client and guide .apk file (from the Builds Quest folder) can be sideloaded on an Oculus Quest whereas the operator build (from the Builds folder) can be used on a computer.

The client and guide can interact with objects using the controllers: 
- objects can be grabbed with the grip button.
- objects can be remote manipulated with the trigger button.

While they are grabbed or manipulated, you can change properties with the buttons: 
- If you are the client, you can change the scale with the left controller, and the color with the right controller.
- If you are the guide, you can change the scale with the left controller, and the layer with the right controller (so that the client can see the object).

The Operator can create and destroy objects through a dashboard in a 3rd room behind a wall.

If you are a Guide and there is not an operator with you, you can make appear a dashboard with the left controller's buttons. You can create and destroy objects thanks to it.